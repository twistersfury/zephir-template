<?php
/**
 * @author    Phoenix <phoenix@twistersfury.com>
 * @license   proprietary
 * @copyright 2016 Twister's Fury
 */

if (!ini_get('extension_name.debug.mode')) {
    header('HTTP/1.1 401 Forbidden');
    exit;
}

if (function_exists('xdebug_info')) {
    xdebug_info();
}

phpinfo();
