<?php
/**
 * @author    Phoenix <phoenix@twistersfury.com>
 * @license   proprietary
 * @copyright 2016 Twister's Fury
 */

use extension__namespace\Kernel;
use Phalcon\Di\FactoryDefault;

$bootstrap   = new Kernel(new FactoryDefault(), dirname(__DIR__));

return $bootstrap->getApplication();
